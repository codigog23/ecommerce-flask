from flask_restx import fields
from flask_restx.reqparse import RequestParser
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema
from app.models.roles_model import RoleModel

class RoleRequestSchema:
    def __init__(self, namespace):
        self.ns = namespace

    def all(self):
        parser = RequestParser()
        parser.add_argument('page', type=int, default=1, location='args')
        parser.add_argument('per_page', type=int, default=5, location='args')
        return parser

    def create(self):
        return self.ns.model('Role Create', {
            'name': fields.String(required=True, max_length=8)
        })
    
    def update(self):
        return self.ns.model('Role Update', {
            'name': fields.String(required=True, max_length=8)
        })

class RoleResponseSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = RoleModel
