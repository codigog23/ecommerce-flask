from flask_restx import fields
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema
from app.models.shopping_cart_model import ShoppingCartModel
from marshmallow.fields import Nested


class ShoppingCartRequestSchema:
    def __init__(self, namespace):
        self.ns = namespace

    def update(self):
        return self.ns.model('Shopping Cart CreateOrUpdate', {
            'product_id': fields.Integer(required=True),
            'quantity': fields.Integer(required=True, min=1)
        })


class ShoppingCartResponseSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = ShoppingCartModel
        include_fk = True
        exclude = ['user_id']

    product = Nested('ProductResponseSchema', many=False, exclude=['description', 'status'])
