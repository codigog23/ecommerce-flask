from flask_restx import fields
from flask_restx.reqparse import RequestParser
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema
from app.models.products_model import ProductModel
from werkzeug.datastructures import FileStorage

class ProductRequestSchema:
    def __init__(self, namespace):
        self.ns = namespace

    def all(self):
        parser = RequestParser()
        parser.add_argument('page', type=int, default=1, location='args')
        parser.add_argument('per_page', type=int, default=5, location='args')
        return parser
    
    def create(self):
        parser = RequestParser()
        parser.add_argument('name', type=str, required=True, location='form')
        parser.add_argument('description', type=str, required=True, location='form')
        parser.add_argument('price', type=float, required=True, location='form')
        parser.add_argument('stock', type=int, required=True, location='form')
        parser.add_argument('image', type=FileStorage, required=True, location='files')
        return parser

class ProductResponseSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = ProductModel
