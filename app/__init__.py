from os import getenv
from flask import Flask
from flask_restx import Api
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_jwt_extended import JWTManager
from flask_mail import Mail
from flask_cors import CORS

from app.config import environment

FLASK_ENV = getenv('FLASK_ENV')
ENVIRONMENT = environment[FLASK_ENV]


app = Flask(__name__)
app.config.from_object(ENVIRONMENT)

CORS(app)

api = Api(
    app,
    title='Flask Lección',
    version='0.1',
    description='Endpoints de la clase de Flask',
    authorizations={
        'Bearer': {
            'name': 'Authorization',
            'in': 'header',
            'type': 'apiKey'
        }
    },
    doc='/swagger-ui'
)

db = SQLAlchemy(app)
migrate = Migrate(app, db)

jwt = JWTManager(app)
mail = Mail(app)
