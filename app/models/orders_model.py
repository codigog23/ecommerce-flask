from app.models.base import BaseModel
from sqlalchemy import Column, Integer, Float, Date, String, ForeignKey
from sqlalchemy.sql import func


class OrderModel(BaseModel):
    __tablename__ = 'orders'

    id = Column(Integer, primary_key=True, autoincrement=True)
    user_id = Column(Integer, ForeignKey('users.id'))
    total_price = Column(Float(precision=2)) # 16.20
    subtotal_price = Column(Float(precision=2))
    igv_price = Column(Float(precision=2))
    create_date = Column(Date, default=func.now())

    checkout_id = Column(String(255), nullable=True)
    checkout_url = Column(String(255), nullable=True)

    external_code = Column(String(120), nullable=True)

    payment_status = Column(String(100), default='pending')
    payment_detail = Column(String(180), nullable=True)
