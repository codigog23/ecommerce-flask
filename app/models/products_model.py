from app.models.base import BaseModel
from sqlalchemy import Column, Integer, String, Text, Float, Boolean


class ProductModel(BaseModel):
    __tablename__ = 'products'

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(120))
    description = Column(Text)
    price = Column(Float(precision=2))
    stock = Column(Integer)
    image = Column(String(255))
    status = Column(Boolean, default=True)
