from app.models.base import BaseModel
from sqlalchemy import Column, Integer, VARCHAR, Boolean, ForeignKey
from sqlalchemy.orm import relationship
from bcrypt import hashpw, gensalt, checkpw

class UserModel(BaseModel): # user_models
    __tablename__ = 'users'

    id = Column(Integer, autoincrement=True, primary_key=True)
    name = Column(VARCHAR(120))
    last_name = Column(VARCHAR(150))
    age = Column(Integer)
    username = Column(VARCHAR(80), unique=True)
    password = Column(VARCHAR(255))
    email = Column(VARCHAR(160), unique=True)
    role_id = Column(Integer, ForeignKey('roles.id'))
    status = Column(Boolean, default=True)

    role = relationship('RoleModel', uselist=False)

    def hash_password(self):
        password_encode = self.password.encode('utf-8')
        password_hash = hashpw(password_encode, gensalt(rounds=10))
        self.password = password_hash.decode('utf-8')

    def check_password(self, password):
        return checkpw(
            password.encode('utf-8'),
            self.password.encode('utf-8')
        )
