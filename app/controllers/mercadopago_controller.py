from app import db
from flask import Response
from app.models.orders_model import OrderModel
from app.helpers.mercadopago import Mercadopago
from http import HTTPStatus


class MercadopagoController:
    def __init__(self):
        self.db = db
        self.model = OrderModel
        self.mercadopago = Mercadopago()

    def update_payment_status(self, payment_id):
        payment_data = self.mercadopago.get_payment_id(payment_id)
        status = payment_data['status']
        status_detail = payment_data['status_detail']
        external_reference = payment_data['external_reference']

        record = self.model.where(external_code=external_reference).first()

        if record:
            record.payment_status = status
            record.payment_detail = status_detail

            self.db.session.add(record)
            self.db.session.commit()

        return Response(status=HTTPStatus.OK)
