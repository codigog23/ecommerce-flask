from app import db
from app.models.users_model import UserModel
from app.schemas.users_schema import UserResponseSchema
from sqlalchemy import or_
from http import HTTPStatus


class UserController:
    def __init__(self):
        self.db = db
        self.model = UserModel
        self.schema = UserResponseSchema

    def fetch_all(self, query):
        # SELECT * FROM users;
        page = query['page']
        per_page = query['per_page']
        search = query.get('q', None)

        # LIKE (SENSIBILIDAD EN MAYUSCULAS Y MINUSCULAS)
        # ILIKE (INSENSIBILIDAD EN MAYUSCULAS Y MINUSCULAS)
        # LIKE 'jeancarlos'
        # Comodines
        # '%jean%' -> busca cualquier cadena que contenga la palabra en mención
        # 'jean%' -> busca cualquier cadena que comience con la palabra en mención
        # '%jean' -> busca cualquier cadena que termine con la palbra en mención

        filters = { 'status': True }

        if search:
            filters[or_] = {
                'name__ilike': f'%{search}%',
                'username__ilike': f'%{search}%',
                'email__ilike': f'%{search}%'
            }

        records = self.model.smart_query(
            filters={**filters}
        ).paginate(
            page=page, per_page=per_page
        )

        response = self.schema(many=True)
        return {
            'results': response.dump(records.items),
            'pagination': {
                'totalRecords': records.total,
                'totalPages': records.pages,
                'perPage': records.per_page,
                'currentPage': records.page
            }
        }, HTTPStatus.OK
    
    def save(self, body):
        try:
            # INSERT INTO users () VALUES ()
            record = self.model.create(**body, commit=False)
            record.hash_password()
            self.db.session.add(record)
            self.db.session.commit()
            return {
                'message': f'El usuario {record.name} se creo con exito'
            }, HTTPStatus.CREATED
        except Exception as e:
            self.db.session.rollback()
            return {
                'message': 'Ocurrio un error',
                'error': e
            }, HTTPStatus.INTERNAL_SERVER_ERROR
        
    def find_by_id(self, id):
        try:
            # SELECT * FROM users WHERE id = 1
                                # columna=valor
            record = self.model.where(id=id, status=True).first()

            if not record:
                return {
                    'message': f'No se encontro un usuario con el ID: {id}'
                }, HTTPStatus.NOT_FOUND

            response = self.schema(many=False)
            return response.dump(record)
        except Exception as e:
            return {
                'message': 'Ocurrio un error',
                'error': e
            }, HTTPStatus.INTERNAL_SERVER_ERROR
        
    def update(self, id, body):
        try:
            record = self.model.where(id=id, status=True).first()

            if not record:
                return {
                    'message': f'No se encontro un usuario con el ID: {id}'
                }, HTTPStatus.NOT_FOUND

            record.update(**body)
            return {
                'message': f'El usuario con el ID: {id} ha sido actualizado'
            }, HTTPStatus.OK
        except Exception as e:
            return {
                'message': 'Ocurrio un error',
                'error': e
            }, HTTPStatus.INTERNAL_SERVER_ERROR
        
    def remove(self, id):
        try:
            record = self.model.where(id=id, status=True).first()

            if not record:
                return {
                    'message': f'No se encontro un usuario con el ID: {id}'
                }, HTTPStatus.NOT_FOUND

            record.update(status=False)
            return {
                'message': f'El usuario con el ID: {id} ha sido eliminado'
            }, HTTPStatus.OK
        except Exception as e:
            return {
                'message': 'Ocurrio un error',
                'error': e
            }, HTTPStatus.INTERNAL_SERVER_ERROR
