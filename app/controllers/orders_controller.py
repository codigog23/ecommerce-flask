from app import db
from app.models.orders_model import OrderModel
from app.models.orders_items_model import OrderItemModel
from app.controllers.shopping_cart_controller import ShoppingCartController
from app.controllers.products_controller import ProductController
from app.helpers.mercadopago import Mercadopago
from flask_jwt_extended import current_user
from secrets import token_hex
from http import HTTPStatus


class OrderController:
    def __init__(self):
        self.db = db
        self.model = OrderModel
        self.model_item = OrderItemModel
        self.user_id = current_user.id

        self.shopping_cart = ShoppingCartController()
        self.products = ProductController()

        self.mercadopago = Mercadopago()

        self.currency = 'PEN'

    def save(self):
        try:
            #1. Traer los productos del carrito (Validar si el carrito esta vacio)
            shopping_cart = self.shopping_cart._fetch_all_items(self.user_id)
            products = shopping_cart['products']
            prices = shopping_cart['prices']

            external_code = token_hex(8)

            if not products:
                raise Exception('El carrito esta vacio')
            #2. Creación del pedido (entidad)
            order = self.model().fill(
                user_id=self.user_id,
                total_price=prices['total'],
                subtotal_price=prices['sub_total'],
                igv_price=prices['igv'],
                external_code=external_code
            ).save(commit=False)

            #2.1. Crear el Link de pago
            payment = self.__create_payment_url(external_code, products)
            payment_url = payment['init_point']

            order.checkout_id = payment['id']
            order.checkout_url = payment_url

            self.db.session.add(order)

            # Usar flush para enviar los cambios a la BD y obtener el ID de la orden
            self.db.session.flush()

            #3. Guardar el detalle del pedido (productos)
            order_items = [
                self.model_item().fill(
                    order_id=order.id,
                    product_id=product['product_id'],
                    price=product['product']['price'],
                    quantity=product['quantity']
                ).save(commit=False)
                for product in products
            ]

            #4. Reducir el stock de los productos
            reduces_stocks = self.products._reduce_stocks(products)
            #5. Limpiar el carrito de compras
            shopping_cart_objects = self.shopping_cart._fetch_all_objects(self.user_id)

            # Guardar el pedido detalle
            self.db.session.add_all(order_items)

            # Actualizamos los stocks
            self.db.session.add_all(reduces_stocks)

            # Agregar la limpieza del carrito
            for cart_object in shopping_cart_objects:
                self.db.session.delete(cart_object)

            # Ejecutar los cambios en la BD
            self.db.session.commit()

            return {
                'message': 'Se creo el pedido con exito',
                'checkout_url': payment_url
            }, HTTPStatus.OK
        except Exception as e:
            self.db.session.rollback()
            return {
                'message': 'Ocurrio un error',
                'error': str(e)
            }, HTTPStatus.INTERNAL_SERVER_ERROR
        
    def __create_payment_url(self, external_code, products):
        payer = {
            'name': current_user.name,
            'surname': current_user.last_name,
            'email': current_user.email
        }

        items = [
            {
                'id': product['product_id'],
                'title': product['product']['name'],
                'picture_url': product['product']['image'],
                'quantity': product['quantity'],
                'currency_id': self.currency,
                'unit_price': product['product']['price']
            }
            for product in products
        ]

        return self.mercadopago.create_preference(payer, items, external_code)
