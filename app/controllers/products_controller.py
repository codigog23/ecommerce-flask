from app import db
from app.models.products_model import ProductModel
from app.schemas.products_schema import ProductResponseSchema
from app.helpers.bucket import Bucket
from http import HTTPStatus


class ProductController:
    def __init__(self):
        self.db = db
        self.model = ProductModel
        self.schema = ProductResponseSchema
        self.bucket = Bucket('flaskecommerce', 'products')

        self.__allowed_extensions = ['jpg', 'jpeg', 'png', 'webp']

    def fetch_all(self, query):
        # nro_pagina
        # limite_filas
        page = query['page']
        per_page = query['per_page']

        records = self.model.where(status=True).paginate(
            page=page, per_page=per_page
        )
        # Objeto paginate
        # page -> Pagina actual
        # per_page -> Total de registros x pagina
        # total -> Total de registros
        # pages -> Total de paginas
        # items -> Lista de objetos
        response = self.schema(many=True)
        return {
            'results': response.dump(records.items),
            'pagination': {
                'totalRecords': records.total,
                'totalPages': records.pages,
                'perPage': records.per_page,
                'currentPage': records.page
            }
        }, HTTPStatus.OK
    
    def save(self, body):
        try:
            image = body.get('image')
            filename, stream = self.__validate_extension_image(image)
            body['image'] = self.bucket.upload_object(filename, stream)

            record = self.model.create(**body)
            return {
                'message': f'El producto {record.name} se creo con exito'
            }, HTTPStatus.CREATED
        except Exception as e:
            return {
                'message': 'Ocurrio un error',
                'error': str(e)
            }, HTTPStatus.INTERNAL_SERVER_ERROR
        
    def find_by_id(self, id):
        try:
            record = self.model.where(id=id, status=True).first()

            if not record:
                return {
                    'message': f'No se encontro el producto con el ID: {id}'
                }, HTTPStatus.NOT_FOUND

            response = self.schema(many=False)
            return response.dump(record)
        except Exception as e:
            return {
                'message': 'Ocurrio un error',
                'error': e
            }, HTTPStatus.INTERNAL_SERVER_ERROR
        
    def update(self, id, body):
        try:
            record = self.model.where(id=id, status=True).first()

            if not record:
                return {
                    'message': f'No se encontro el producto con el ID: {id}'
                }, HTTPStatus.NOT_FOUND

            record.update(**body)
            return {
                'message': f'El producto con el ID: {id} ha sido actualizado'
            }, HTTPStatus.OK
        except Exception as e:
            return {
                'message': 'Ocurrio un error',
                'error': e
            }, HTTPStatus.INTERNAL_SERVER_ERROR
        
    def remove(self, id):
        try:
            record = self.model.where(id=id, status=True).first()

            if not record:
                return {
                    'message': f'No se encontro el producto con el ID: {id}'
                }, HTTPStatus.NOT_FOUND

            record.update(status=False)
            return {
                'message': f'El producto con el ID: {id} ha sido eliminado'
            }, HTTPStatus.OK
        except Exception as e:
            return {
                'message': 'Ocurrio un error',
                'error': e
            }, HTTPStatus.INTERNAL_SERVER_ERROR
        
    def __validate_extension_image(self, image):
        filename = image.filename
        stream = image.stream
        extension = filename.split('.')[1] # manzana.png -> ['manzana', 'png']

        if extension not in self.__allowed_extensions:
            raise Exception('El tipo de archivo no tiene el formato permitido')
        
        return filename, stream
    
    def _reduce_stocks(self, products):
        updates = []
        for product in products:
            record = self.model.where(id=product['product_id']).first()
            new_stock = record.stock - product['quantity']
            record.fill(stock=new_stock).save(commit=False)
            updates.append(record)
        return updates
