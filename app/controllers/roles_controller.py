from app import db
from app.models.roles_model import RoleModel
from app.schemas.roles_schema import RoleResponseSchema
from http import HTTPStatus


class RoleController:
    def __init__(self):
        self.db = db
        self.model = RoleModel
        self.schema = RoleResponseSchema

    def fetch_all(self, query):
        # nro_pagina
        # limite_filas
        page = query['page']
        per_page = query['per_page']

        records = self.model.where(status=True).paginate(
            page=page, per_page=per_page
        )
        # Objeto paginate
        # page -> Pagina actual
        # per_page -> Total de registros x pagina
        # total -> Total de registros
        # pages -> Total de paginas
        # items -> Lista de objetos
        response = self.schema(many=True)
        return {
            'results': response.dump(records.items),
            'pagination': {
                'totalRecords': records.total,
                'totalPages': records.pages,
                'perPage': records.per_page,
                'currentPage': records.page
            }
        }, HTTPStatus.OK
    
    def save(self, body):
        try:
            record = self.model.create(**body)
            return {
                'message': f'El rol {record.name} se creo con exito'
            }, HTTPStatus.CREATED
        except Exception as e:
            return {
                'message': 'Ocurrio un error',
                'error': e
            }, HTTPStatus.INTERNAL_SERVER_ERROR
        
    def find_by_id(self, id):
        try:
            record = self.model.where(id=id, status=True).first()

            if not record:
                return {
                    'message': f'No se encontro el rol con el ID: {id}'
                }, HTTPStatus.NOT_FOUND

            response = self.schema(many=False)
            return response.dump(record)
        except Exception as e:
            return {
                'message': 'Ocurrio un error',
                'error': e
            }, HTTPStatus.INTERNAL_SERVER_ERROR
        
    def update(self, id, body):
        try:
            record = self.model.where(id=id, status=True).first()

            if not record:
                return {
                    'message': f'No se encontro el rol con el ID: {id}'
                }, HTTPStatus.NOT_FOUND

            record.update(**body)
            return {
                'message': f'El rol con el ID: {id} ha sido actualizado'
            }, HTTPStatus.OK
        except Exception as e:
            return {
                'message': 'Ocurrio un error',
                'error': e
            }, HTTPStatus.INTERNAL_SERVER_ERROR
        
    def remove(self, id):
        try:
            record = self.model.where(id=id, status=True).first()

            if not record:
                return {
                    'message': f'No se encontro el rol con el ID: {id}'
                }, HTTPStatus.NOT_FOUND

            record.update(status=False)
            return {
                'message': f'El rol con el ID: {id} ha sido eliminado'
            }, HTTPStatus.OK
        except Exception as e:
            return {
                'message': 'Ocurrio un error',
                'error': e
            }, HTTPStatus.INTERNAL_SERVER_ERROR

