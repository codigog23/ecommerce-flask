from app import db
from app.models.shopping_cart_model import ShoppingCartModel
from app.schemas.shopping_cart_schema import ShoppingCartResponseSchema
from flask_jwt_extended import current_user
from http import HTTPStatus


class ShoppingCartController:
    def __init__(self):
        self.db = db
        self.model = ShoppingCartModel
        self.schema = ShoppingCartResponseSchema
        self.user_id = current_user.id
        self.igv = 0.18

    def fetch_all(self):
        return self._fetch_all_items(self.user_id), HTTPStatus.OK

    def update(self, body):
        try:
            product_id = body['product_id']

            record = self.model.where(
                product_id=product_id,
                user_id=self.user_id
            ).first()

            if record:
                record.update(**body)
            else:
                body['user_id'] = self.user_id
                record = self.model.create(**body, commit=False)

            self.db.session.add(record)
            self.db.session.commit()

            return {
                'message': 'Se actualizo el carrito de compras'
            }, HTTPStatus.OK
        except Exception as e:
            self.db.session.rollback()
            return {
                'message': 'Ocurrio un error',
                'error': e
            }, HTTPStatus.INTERNAL_SERVER_ERROR
        
    def remove(self, id):
        try:
            record = self.model.where(
                product_id=id,
                user_id=self.user_id
            ).first()

            if not record:
                return {
                    'message': 'No se encontro el producto en el carrito de compras'
                }, HTTPStatus.NOT_FOUND

            record.delete()
            return {
                'message': 'Se elimino el producto con exito'
            }, HTTPStatus.OK
        except Exception as e:
            return {
                'message': 'Ocurrio un error',
                'error': e
            }, HTTPStatus.INTERNAL_SERVER_ERROR
        
    def _fetch_all_items(self, user_id):
        records = self.model.where(user_id=user_id).all()
        response = self.schema(many=True)
        data = response.dump(records)
        prices = {
            'total': 0,
            'sub_total': 0,
            'igv': 0
        }

        if data:
            for item in data:
                price = item['product']['price']
                quantity = item['quantity']
                product_price_total = round(price * quantity, 2)
                prices['sub_total'] += product_price_total
                item['price_total'] = product_price_total

            prices['igv'] = round(
                prices['sub_total'] * self.igv, 2
            )

            prices['total'] = round(
                prices['sub_total'] + prices['igv'], 2
            )
        
        return {
            'products': data,
            'prices': prices
        }
    
    def _fetch_all_objects(self, user_id):
        return self.model.where(user_id=user_id).all()
