from app import jwt
from app.models.users_model import UserModel


@jwt.user_lookup_loader
def user_lookup_callback(header, data):
    identity = data['sub']
    return UserModel.where(id=identity).first()
