from os import getenv
from requests import post, get


class Mercadopago:
    def __init__(self):
        self.access_token = getenv('MERCADOPAGO_ACCESSTOKEN')
        self.headers = {
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {self.access_token}'
        }
        self.base_url = 'https://api.mercadopago.com'
        self.currency = 'PEN'

    def create_preference(self, payer, items, external_reference):
        url = f'{self.base_url}/checkout/preferences'
        body = {
            'payer': payer,
            'items': items,
            # 'back_urls':{
            #     'failure': 'http://127.0.0.1:5000/failure',
            #     'pending': 'http://127.0.0.1:5000/pending',
            #     'success': 'http://127.0.0.1:5000/success'
            # },
            'external_reference': external_reference
        }
        response = post(url, json=body, headers=self.headers)
        return response.json()
    
    def get_payment_id(self, payment_id):
        url = f'{self.base_url}/v1/payments/{payment_id}'
        response = get(url, headers=self.headers)
        return response.json()
