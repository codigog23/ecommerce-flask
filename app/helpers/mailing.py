from os import getenv
from app import mail
from flask_mail import Message
from flask import render_template


class Mailing:
    def __init__(self):
        self.sender = ('Flask Boilerplate', getenv('MAIL_USERNAME'))

    def mail_reset_password(self, recipient, name, password):
        body = render_template('reset_password.html', name=name, password=password)
        return self.__send_mail(
            f'Reinicio de contraseña - {name}',
            [recipient],
            body
        )
    
    def __send_mail(self, subject, recipients, body):
        message = Message(
            subject=subject,
            sender=self.sender,
            recipients=recipients,
            html=body
        )
        return mail.send(message)
