from app import api
from flask_restx import Resource
from flask import request
from app.controllers.mercadopago_controller import MercadopagoController
from http import HTTPStatus

mercadopago_ns = api.namespace(
    name='Mercadopago',
    description='Endpoints para validación con MELI',
    path='/mercadopago'
)

@mercadopago_ns.route('')
class MercadopagoRouter(Resource):
    def post(self):
        ''' Webhook para pagos de mercadopago '''
        # headers -> request.headers.get('x-signature')
        query = request.args.to_dict()
        payment_id = query['data.id']
        controller = MercadopagoController()
        return controller.update_payment_status(payment_id)
