from app import api
from flask_restx import Resource
from flask_jwt_extended import jwt_required
from app.controllers.orders_controller import OrderController


order_ns = api.namespace(
    name='Ordenes de venta',
    description='Endpoint para el modulo de ordenes',
    path='/orders'
)

@order_ns.route('')
@order_ns.doc(security='Bearer')
class OrderRouter(Resource):
    @jwt_required()
    def post(self):
        ''' Generar orden de venta '''
        controller = OrderController()
        return controller.save()
