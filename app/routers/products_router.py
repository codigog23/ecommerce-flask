from app import api
from flask_restx import Resource
from app.schemas.products_schema import ProductRequestSchema
from app.controllers.products_controller import ProductController


product_ns = api.namespace(
    name='Productos',
    description='Endpoint del modulo productos',
    path='/products'
)

schema_request = ProductRequestSchema(product_ns)


@product_ns.route('')
class ProductRouter(Resource):
    @product_ns.expect(schema_request.all())
    def get(self):
        query = schema_request.all().parse_args()
        controller = ProductController()
        return controller.fetch_all(query)

    @product_ns.expect(schema_request.create(), validate=True)
    def post(self):
        body = schema_request.create().parse_args()
        controller = ProductController()
        return controller.save(body)

@product_ns.route('/<int:id>')
class ProductByIdRouter(Resource):
    def get(self, id):
        pass

    def put(self, id):
        pass

    def delete(self, id):
        pass
