from app import api
from flask_restx import Resource
from flask_jwt_extended import jwt_required
from flask import request
from app.controllers.shopping_cart_controller import ShoppingCartController
from app.schemas.shopping_cart_schema import ShoppingCartRequestSchema


shopping_ns = api.namespace(
    name='Carrito de compra',
    description='Endpoints del modulo shopping_cart',
    path='/shopping_cart'
)

schema_request = ShoppingCartRequestSchema(shopping_ns)


@shopping_ns.route('')
@shopping_ns.doc(security='Bearer')
class ShoppingCartRouter(Resource):
    @jwt_required()
    def get(self):
        ''' Listar productos del carrito '''
        controller = ShoppingCartController()
        return controller.fetch_all()

    @jwt_required()
    @shopping_ns.expect(schema_request.update(), validate=True)
    def put(self):
        ''' Crear o actualizar un producto del carrito '''
        controller = ShoppingCartController()
        return controller.update(request.json)
    
@shopping_ns.route('/<int:product_id>')
@shopping_ns.doc(security='Bearer')
class ShoppingCartByProductIdRouter(Resource):
    @jwt_required()
    def delete(self, product_id):
        ''' Eliminar un producto del carrito '''
        controller = ShoppingCartController()
        return controller.remove(product_id)
