![logo](https://www.python.org/static/community_logos/python-logo-generic.svg)

# Flask Ecommerce

---

## Modelos

- Usuarios (users)

| columna   | tipo         | constraint           |
| --------- | ------------ | -------------------- |
| id        | SERIAL       | PRIMARY KEY          |
| name      | VARCHAR(120) | NOT NULL             |
| last_name | VARCHAR(150) | NOT NULL             |
| age       | INTEGER      | NOT NULL             |
| username  | VARCHAR(80)  | UNIQUE               |
| password  | VARCHAR(255) | NOT NULL             |
| email     | VARCHAR(160) | UNIQUE               |
| role_id   | INTEGER      | FOREIGN KEY NOT NULL |
| status    | BOOLEAN      | DEFAULT TRUE         |

- Roles (roles)

| columna | tipo    | constraint   |
| ------- | ------- | ------------ |
| id      | SERIAL  | PRIMARY KEY  |
| name    | CHAR(8) | NOT NULL     |
| status  | BOOLEAN | DEFAULT TRUE |

- Productos (products)

| columna     | tipo         | constraint   |
| ----------- | ------------ | ------------ |
| id          | SERIAL       | PRIMARY KEY  |
| name        | VARCHAR(120) | NOT NULL     |
| description | TEXT         | NOT NULL     |
| price       | NUMERIC      | NOT NULL     |
| stock       | INTEGER      | NOT NULL     |
| image       | VARCHAR(255) | NOT NULL     |
| status      | BOOLEAN      | DEFAULT TRUE |

- Carrito de Compras (shopping_cart)

| columna    | tipo    | constraint           |
| ---------- | ------- | -------------------- |
| id         | SERIAL  | PRIMARY KEY          |
| product_id | INTEGER | FOREIGN KEY NOT NULL |
| user_id    | INTEGER | FOREIGN KEY NOT NULL |
| quantity   | INTEGER | NOT NULL             |

- Pedido de Venta (orders)

| columna        | tipo    | constraint           |
| -------------- | ------- | -------------------- |
| id             | SERIAL  | PRIMARY KEY          |
| user_id        | INTEGER | FOREIGN KEY NOT NULL |
| total_price    | NUMERIC | NOT NULL             |
| subtotal_price | NUMERIC | NOT NULL             |
| igv_price      | NUMERIC | NOT NULL             |
| create_date    | DATE    | DEFAULT DATE_NOW     |

- Detalle de la venta (orders_items)

| columna    | tipo    | constraint           |
| ---------- | ------- | -------------------- |
| id         | SERIAL  | PRIMARY KEY          |
| order_id   | INTEGER | FOREIGN KEY NOT NULL |
| product_id | INTEGER | FOREIGN KEY NOT NULL |
| price      | NUMERIC | NOT NULL             |
| quantity   | INTEGER | NOT NULL             |

## Environment (.env)

```py
FLASK_APP='main.py'
FLASK_RUN_HOST=127.0.0.1
FLASK_RUN_PORT=5000
FLASK_DEBUG=True
FLASK_ENV='development'

DATABASE_URL='postgresql://postgres:mysql@localhost:5432/flask_boilerplate'
SECRET_KEY='tecsup'

MAIL_SERVER='smtp.gmail.com'
MAIL_PORT=587
MAIL_USE_TLS=True
MAIL_USERNAME=''
MAIL_PASSWORD=''

AWS_ACCESS_KEY_ID=''
AWS_ACCESS_KEY_SECRET=''
AWS_REGION=''

MERCADOPAGO_ACCESSTOKEN=''
```
